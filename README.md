# bemo audits

## 12-2023 CertiK Audit: Liquid Staking 

- Total Findings: 17 (16 Resolved, 1 Acknowledged)
- Critical Fingings: 1 (1 Resolved)
- Major Findings: 1 (1 Acknowledged)
- Medium Fingings: 2 (2 Resolved) 
- Minor Fingings: 9 (9 Resolved)
- Optimization Findings: 1 (1 Resolved)
- Informational Findings 3 (3 Resolved)


See [full report](CertiK%20Liquid%20Staking%20Protocol.pdf) for more details.
